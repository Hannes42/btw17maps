# German federal election 2017 maps

A list of URLs and WARC archives of maps of the German federal elections 2017 in online media.

Focused on visualisations on a national level, feel free to add local ones though.
Interactive and non-interactive maps are welcome. Good ones, bad ones. Anything, really. For science! Potentially...

Please feel welcome to use this collection for your own endeavours. :)

You can browse WARC files with https://github.com/webrecorder/webrecorderplayer-electron/releases/

Alternatively check out our awesome http://www.archiveteam.org/index.php?title=The_WARC_Ecosystem

Captured using webrecorder.io

- Using the native recording with a clean Chromium incognito window on Linux when possible, using a
vertical monitor so it might have used some responsive versions.
- Using remote Firefox if needed (webgl, and apparently something with iframes?)
- I did try to find crucial interaction (like switching between parties on bild.de) but kept it to a minimum. I never zoomed or panned.

For recording titles (otherwise I had trouble with webrecorder.io):

1. Remove protocol
1. Replace all / with " "

For example 
http://www.spiegel.de/politik/deutschland/bundestagswahl-2017-wo-eine-jamaika-koalition-eine-mehrheit-hat-und-wo-nicht-a-1168632.html becomes "www.spiegel.de politik deutschland bundestagswahl-2017-wo-eine-jamaika-koalition-eine-mehrheit-hat-und-wo-nicht-a-1168632.html"


| URL | WARC | Notes |
|-----|------|-------|
| http://wahlatlas.net/btw/17/ | ✓ | hard to capture without lots of interaction i think |
| http://www.augsburger-allgemeine.de/politik/Interaktive-Karte-Alle-Wahlkreis-Ergebnisse-aus-der-Region-id42762716.html |  | does not finish loading ... try remote? |
| http://www.bild.de/politik/inland/bundestagswahl2017/wie-hat-mein-wahlkreis-gewaehlt-53319146.bild.html |  | does not finish loading ... try remote? |
| http://www.express.de/koeln/bundestagswahl-2017-so-haben-die-koelner-in-ihrem-veedel-gewaehlt-28481080 | ✓ |  |
| http://www.spiegel.de/politik/deutschland/bundestagswahl-2017-alle-ergebnisse-im-ueberblick-a-1167247.html | ✓ |  |
| http://www.spiegel.de/politik/deutschland/bundestagswahl-2017-wo-eine-jamaika-koalition-eine-mehrheit-hat-und-wo-nicht-a-1168632.html | ✓ |  |
| http://www.spiegel.de/politik/deutschland/bundestagswahl-wo-die-wahl-entschieden-wurde-a-1168515.html | ✓ | recorded with server-side Firefox for webgl |
| http://www.sueddeutsche.de/politik/bundestagswahl-von-afd-hochburgen-und-welkenden-spd-landschaften-1.3653498 | ✓ |  |
| http://www.wz.de/home/politik/interaktive-karte-so-hat-nrw-gewaehlt-1.2523012 | ✓ |  |
| http://www.zeit.de/politik/deutschland/2017-09/wahlverhalten-bundestagswahl-wahlbeteiligung-waehlerwanderung | ✓ |  |
| https://interaktiv.abendblatt.de/hamburgwahlkarte2017/ | ✓ |  |
| https://interaktiv.morgenpost.de/analyse-bundestagswahl-2017/ | ✓ |  |
| https://interaktiv.morgenpost.de/deutschlandwahlkarte2017/ | ✓ |  |
| https://interaktiv.rp-online.de/bundestagswahl-17/ergebnisse/duesseldorf/ | ✓ |  |
| https://wahl.tagesschau.de/wahlen/2017-09-24-BT-DE/index.shtml | ✓ | recorded with server-side Firefox for the map |
| https://wahl.tagesspiegel.de/2017/karten/wahlbezirke/ | ✓ |  |
| https://wahl.tagesspiegel.de/2017/karten/wahlkreise/ | ✓ |  |
| https://www.bloomberg.com/graphics/2017-germany-post-election-analysis/ | ✓ |  |
| https://www.br.de/bundestagswahl/tableau-btw17-wahlkreise-bayern-100.html | ✓ |  |
| https://www.merkur.de/politik/bundestagswahl-2017-hochrechnung-sitzverteilung-sowie-ergebnisse-aller-wahlkreise-und-gemeinden-in-deutschland-zr-8516887.html | ✓ | recorded with server-side Firefox for the map |
| https://www.nzz.ch/international/bundestagswahl-deutschland-wo-die-parteien-gewonnen-haben-wo-sie-verloren-haben-ld.1316297 | ✓ |  |
| https://www.welt.de/politik/deutschland/article168997055/Das-sind-die-Hochburgen-der-Parteien-in-Deutschland.html | ✓ |  |
| https://www.welt.de/politik/deutschland/article168997191/Diese-Karten-zeigen-wie-krass-sich-Deutschland-veraendert-hat.html | ✓ |  |